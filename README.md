# DevOps with GitLab CI Course - Build Pipelines and Deploy to AWS

👋 Welcome to this GitLab CI course available on freeCodeCamp.

## Getting started

- **Check the** [**course notes**](docs/course-notes.md)
- Watch the [GitLab CI course on freecodecamp](https://www.youtube.com/watch?v=PGyhBwLyK2U)
- Something is wrong? If you want to report an issue or need help with something, you can create an issue. Please include screenshots and as many details as possible. [**Submit an issue.**](https://gitlab.com/gitlab-course-public/freecodecamp-gitlab-ci/-/issues/new?issue%5Bmilestone_id%5D=)
[![GitLab course on YouTube](https://img.youtube.com/vi/PGyhBwLyK2U/maxresdefault.jpg)](https://www.youtube.com/watch?v=PGyhBwLyK2U)

# GitLab CI Course - FreeCodeCamp  

## Links

1. [Classentral](https://www.classcentral.com/classroom/freecodecamp-devops-with-gitlab-ci-course-build-pipelines-and-deploy-to-aws-104862)
2. [DevOps with GitLab CI Course - Build Pipelines and Deploy to AWS - YouTube](https://www.youtube.com/watch?v=PGyhBwLyK2U)
3. [Course notes](https://gitlab.com/gitlab-course-public/freecodecamp-gitlab-ci/-/blob/main/docs/course-notes.md)
4. [Forked GitLab repo](https://gitlab.com/m.zn/freecodecamp-gitlab-ci)
5. [Learn GitLab with tutorials  GitLab](https://docs.gitlab.com/ee/tutorials/)
6. [Run Playwright Tests via GitLab](https://medium.com/@info_70421/running-playwright-tests-via-gitlab-ci-cd-pipeline-572e1d95f6f2)
7. [The Phoenix Project: 10 Years of Transformation]([https://](https://itrevolution.com/articles/the-phoenix-project-10-years-of-transformation/))

## Progress  

- [x] ⌨️ Lesson 1 - Welcome (0:00:00​)
- [x] ⌨️ Lesson 2 - Your first GitLab project (0:03:03​)
- [x] ⌨️ Lesson 3 - Your first pipeline (0:13:00​)
- [x] ⌨️ Lesson 4 - Help, my pipeline is not working (0:23:32)
- [x] ⌨️ Lesson 5 - What is YAML? (0:26:22)
- [x] ⌨️ Lesson 6 - What is a shell? (0:35:12)
- [x] ⌨️ Lesson 7 - GitLab architecture (0:37:50)
- [x] ⌨️ Lesson 8 - Pipeline stages (0:43:14)
- [x] ⌨️ Lesson 9 - Why do pipelines fail? (0:48:11)
- [x] ⌨️ Lesson 10 - Job artifacts (0:52:34)
- [x] ⌨️ Lesson 11 - Testing the build (0:59:02)
- [x] ⌨️ Lesson 12 - Variables (1:04:33)
- [x] ⌨️ Lesson 13 - What is DevOps (1:10:27)

⭐️ Unit 2 - Continuous Integration with GitLab CI

- [x] ⌨️ Lesson 1 - Unit overview (1:16:53)
- [x] ⌨️ Lesson 2 - Your first GitLab project (1:18:41)
- [x] ⌨️ Lesson 3 - Building the project (1:22:05)

## Unit 3. Lesson 4 - Assignment (1:33:10)

## Unit 3. Lesson 5 - Assignment solution (1:34:43)

## Unit 3. Lesson 6 - How to integrate changes? (1:40:24)

## Unit 3. Lesson 7 - Merge requests (1:43:50)

## Unit 3. Lesson 8 - Code review (1:51:38)

## Unit 3. Lesson 9 - Integration tests (1:56:29)

## Unit 3. Lesson 10 - How to structure a pipeline (2:10:53)

⭐️ Unit 3 - Continuous Deployment with GitLab & AWS

## Unit 3. Lesson 1 - Unit overview (2:16:41)

## Unit 3. Lesson 2 - A quick introduction to AWS (2:17:14)

## Unit 3. Lesson 3 - AWS S3 (2:20:57)

## Unit 3. Lesson 4 - AWS CLI (2:23:35)

## Unit 3. Lesson 5 - Uploading a file to S3 (2:29:04)

## Unit 3. Lesson 6 - Masking & protecting variables (2:33:00)

## Unit 3. Lesson 7 - Identity management with AWS IAM (2:38:49)

## Unit 3. Lesson 8 - Uploading multiple files to S3 (2:47:54)

## Unit 3. Lesson 9 - Hosting a website on S3 (2:53:15)

## Unit 3. Lesson 10 - Controlling when jobs run (3:00:06)

## Unit 3. Lesson 11 - Post-deployment testing (3:07:03)

## Unit 3. Lesson 12 - What is CI/CD? (3:13:01)

## Unit 3. Lesson 13 - Assignment (3:16:47)

## Unit 3. Lesson 14 - Assignment solution (3:17:26)

## Unit 3. Lesson 15 - Environments (3:24:40)

## Unit 3. Lesson 16 - Reusing configuration (3:33:52)

## Unit 3. Lesson 17 - Assignment (3:36:57)

## Unit 3. Lesson 18 - Assignment solution (3:40:53)

## Unit 3. Lesson 19 - Continuous Delivery pipeline (3:44:15)

⭐️ Unit 4 - Deploying a dockerized application to AWS

## Unit 3. Lesson 1 - Unit overview (3:48:129)

## Unit 3. Lesson 2 - Introduction to AWS Elastic Beanstalk (3:49:25)

## Unit 3. Lesson 3 - Creating a new AWS Elastic Beanstalk application (3:51:48)

## Unit 3. Lesson 4 - Creating the Dockerfile (3:59:02)

## Unit 3. Lesson 5 - Building the Docker image (4:02:12)

## Unit 3. Lesson 6 - Docker container registry (4:09:27)

## Unit 3. Lesson 7 - Testing the container (4:15:59)

## Unit 3. Lesson 8 - Private registry authentication (4:20:04)

## Unit 3. Lesson 9 - Deploying to AWS Elastic Beanstalk (4:34:18)

## Unit 3. Lesson 10 - Post-deployment testing (4:45:54)

## Unit 3. Lesson 11 - CI/CD recap (4:50:29)

⭐️ Unit 5 - Conclusion

## Unit 3. Lesson 1 - Final assignment (4:51:37)

## Unit 3. Lesson 2 - Conclusion (4:55:16)

## Lesson 2 - Your first GitLab project (0:03:03​)

1. Create GitLab account
2. Create private project
3. Change settings > Preferences  > Syntax highlighting > Monokai
4. On: Render whitespace characters in the web IDE
5. Create new file `.gitlab-ci.yml`
6. Go to Build > Pipelines > Run pipeline
7. Check pipeline logs

Push an existing Git repository

```bash
cd existing_repo
git remote add origin https://gitlab.com/mzn2/gitlabci.git
git branch -M main
git push -uf origin main

```

```yml
test:
    script: echo "Hello world"

```

## Lesson 3 - Your first pipeline (0:13:00​)

![pipeline](./attachments/pipeline1.jpg)

1. Add commands to write text file
2. By default pipeline uses Ruby Docker image
3. Add alternative Docker image - Linux `alpine`
4. Commit changes
5. Check pipeline logs

```yml
build laptop:
    image: alpine
    script: 
        - echo "Building laptop"
        - mkdir build
        - touch build/$BUILD_FILE_NAME
        - echo "Mainboard" >> build/$BUILD_FILE_NAME
        - cat build/$BUILD_FILE_NAME
        - echo "Keyboard" >> build/$BUILD_FILE_NAME
        - cat build/$BUILD_FILE_NAME
```  

Output

```bash
Running with gitlab-runner 16.6.0~beta.105.gd2263193 (d2263193)
  on blue-2.saas-linux-small-amd64.runners-manager.gitlab.com/default XxUrkriX, system ID: s_f46a988edce4
  feature flags: FF_USE_IMPROVED_URL_MASKING:true
Resolving secrets
00:00
Preparing the "docker+machine" executor
00:05
Using Docker executor with image alpine ...
Pulling docker image alpine ...
Using docker image sha256:f8c20f8bbcb684055b4fea470fdd169c86e87786940b3262335b12ec3adef418 for alpine with digest alpine@sha256:51b67269f354137895d43f3b3d810bfacd3945438e94dc5ac55fdac340352f48 ...
Preparing environment
00:01
Running on runner-xxurkrix-project-53471482-concurrent-0 via runner-xxurkrix-s-l-s-amd64-1704186592-2d594c12...
Getting source from Git repository
00:01
Fetching changes with git depth set to 20...
Initialized empty Git repository in /builds/mzn2/gitlabci/.git/
Created fresh repository.
Checking out 341525e5 as detached HEAD (ref is main)...
Skipping Git submodules setup
$ git remote set-url origin "${CI_REPOSITORY_URL}"
Executing "step_script" stage of the job script
00:00
Using docker image sha256:f8c20f8bbcb684055b4fea470fdd169c86e87786940b3262335b12ec3adef418 for alpine with digest alpine@sha256:51b67269f354137895d43f3b3d810bfacd3945438e94dc5ac55fdac340352f48 ...
$ echo "Building laptop"
Building laptop
$ mkdir build
$ touch build/$BUILD_FILE_NAME
$ echo "Mainboard" >> build/$BUILD_FILE_NAME
$ cat build/$BUILD_FILE_NAME
Mainboard
$ echo "Keyboard" >> build/$BUILD_FILE_NAME
$ cat build/$BUILD_FILE_NAME
Mainboard
Keyboard
Cleaning up project directory and file based variables
00:01
Job succeeded
```

## Lesson 4 - Help, my pipeline is not working

1. Missing colon after job in yml file
2. Missing space between dash and command
3. Incorrect levele indentation (dfault 4 spaces )

## Lesson 5 - What is YAML

yaml example

```yml
person:
    name: John
    age: 23

    hobbies:
        - sports
        - YouTube
        - hiking
    address: 
        street: 123 Mayfield Ave.
    experience:
        - title: Junior Dev
          period: 2000-2005
        - title: Senior Dev 
          period: since 2005
```

It is important to use defined keywords and definitions.

## Lesson 7 - GitLab architecture

![GitLab-architecture](attachments/L07/GitLab-architecture.pngGitLab-architecture.png)

GitLab runner has relatively simple installation.
Can be run even on laptop.

- GitLab project > Settings > Runners  

Shared runners are shared between all GitLab users. F

## Lesson 8 - Pipeline stages  

```yaml
variables:
  ENV: 'dev'

stages:
  - setup
  - test

setup data: 
  stage: setup
  script:
    - echo "Run script from setup"

run_playwright_smoke_tests:
  stage: test
  image: mcr.microsoft.com/playwright:v1.41.1-jammy
  script:
    - echo "Run script with $ENV environment from test stage"

```

## Lesson 9 - Why do pipelines fail?

Pipeline failed:
Exit code - 1  

Try to check the last command executing before fail occured.
What is the Docker image used?

## Lesson 10 - Job artifacts

Job artifacts allow sharing outputs between different jobs in pipeline. For example files.

Use `artifacts` and `paths` to create artifact so that it can be used in other pipeline jobs.

```yaml
build laptop:
    ...
    artifacts:
        paths:
            - build



```

```yaml
stages:
    - build
    - test

build laptop:
    image: alpine
    stage: build
    script: 
        - echo "Building a laptop"
        - mkdir build
        - touch build/$BUILD_FILE_NAME
        - echo "Mainboard" >> build/$BUILD_FILE_NAME
        - cat build/$BUILD_FILE_NAME
        - echo "Keyboard" >> build/$BUILD_FILE_NAME
    artifacts:
        paths:
            - build

test laptop:
    image: alpine
    stage: test
    script:
        - test -f build/$BUILD_FILE_NAME
```

## Lesson 11 - Testing the build

Use `grep` to test contents of the file contain expected word so that this can be used to verify that code change does not break functionality.

```yaml

   - grep "Display" build/$BUILD_FILE_NAME

````

## Lesson 12 - Variables

Use `variables` as a global variable to specify file name so that file name can be changed in one place only  and take effect in multiple parts of pipeline.

```yaml
stages:
    - build
    - test
variables:
  BUILD_FILE_NAME: laptop.txt

build laptop:
    image: alpine
    stage: build
    script: 
        - echo "Building a laptop"
        - mkdir build
        - touch build/$BUILD_FILE_NAME
        - echo "Mainboard" >> build/$BUILD_FILE_NAME
        - cat build/$BUILD_FILE_NAME
        - echo "Keyboard" >> build/$BUILD_FILE_NAME
    artifacts:
        paths:
            - build

test laptop:
    image: alpine
    stage: test
    script:
        - test -f build/$BUILD_FILE_NAME
```

## Lesson 13 - What is DevOps

- DevOps is not a standard and does not have an universally agreed definition
- DevOps is not a standard or a tool, but a set of practices
- DevOps uses automation to that help us build successful Products
- DevOps requires a different mindset and works really well with Agile & Scrum

Automate as much as possible to save time. Put that time in a good use.

## Setting env variables in CI pipeline and using in Playwright tests  

```yaml
variables:
  ENV: 'basewcdev'
  CI: 'true'

```

playwright.config.ts

```json
  {
    video: retain-on-failure,
    headless: process.env.CI === 'true' ? true : false,
  }

```

## Unit 2. Lesson 4 - Assignment

1. Create 2 jobs in pipeline
2. Job: verify that file is present in `build/index.html`
3. Run unit tests `yarn test`

## Unit 2. Lesson 5 - Assignment solution

Building and running site  

```bash
npm install --global serve
serve --version
serve -s build

```

yarn install did not work

Solution for running tests

```shell
stages:
  - build
  - test



build website:
    stage: build
    image: node:16-alpine
    script:
        - yarn install
        - yarn build
    artifacts:
        when: always
        paths:
            - build

test index.html:
    stage: test
    image: alpine
    script:
        test -f build/index.html

run unit tests:
    stage: test
    image: node:16-alpine
    script:
        - yarn install
        - CI=true yarn test src

```

## Unit 2. Lesson 6 - How to integrate changes?

1. Create new branch
2. Make changes
3. Run tests in branch
4. Create merge request
5. Merge branch if tests passed and pipeline succeeded  

## Unit 2. Lesson 7 - Merge requests (1:43:50)

Set merge method in GitLab.

Project > Settings > Merge requests > Merge method > Fast forward

Squash commits.
When you push multiple changes to branch, instead of pushing all changes we squash them, meaning only one commit will be pushed. Git history will be easier to read.

Merge checks settings

- Pipeline must succeed
- To merge something, pipeline should succeed first.

Protect main branch

Settings > Repository > Protected branches > Allowed to push and merge > no one

Unit test and linter are not dependant on build.

GitLab comes with predefined stages: .pre, build, test, post, deploy

```yaml

stages:
  - build
  - test

build website:
    stage: build
    image: node:16-alpine
    script:
        - yarn install
        - yarn build
    artifacts:
        when: always
        paths:
            - build

linter:
    stage: .pre
    image: node:16-alpine
    script:
        - yarn install
        - yarn lint

test index.html:
    stage: test
    image: alpine
    script:
        test -f build/index.html

run unit tests:
    stage: .pre
    image: node:16-alpine
    script:
        - yarn install
        - CI=true yarn test src


```

## Unit 2. Lesson 8 - Code review

Merge request is created.
Can leave comments.
Changes can be integrated.
MR can be approved.
changes are merged when pipeline is succeeded.

## Unit 2. Lesson 9 - Integration tests

Unit test do not require building app.
Integration tests will check that website is built and running.

To disable job add dot:

```yaml

.run unit tests:
    stage: .pre
    image: node:16-alpine
    script:
        - yarn install
        - CI=true yarn test src


```

## Unit 2. Lesson 10 - How to structure a pipeline

Core principles :

1. Fail fast. Add faster stages in the beginning such as lint or unit tests
2. Put parallel jobs of similar size. If one fails, another will have to finish.
3. Keep in mind dependencies. For exampple, integration tests only affter build stage.

## Unit 3. Lesson 1. Overview

## Unit 3. Lesson 3 - AWS S3

## Unit 3. Overview

## Unit 3. Lesson 3 - A quick introduction to AWS (2:17:14)

## Unit 3. Lesson 3 - AWS S3

S3 - simple storage service.
S3 can be used when web app is a static app, no computing required, no database.
Files are stored in Buckets (folders).

1. Create bucket. `mkzin-20240616`

## Unit 3. Lesson 4 - AWS CLI

1. Find docker image with AWS CLI  on `https://hub.docker.com/`
2. Create new job in GitLab pipeline
3. Recommended: add tag in docker image.

By default a program is running on that image and using entrypoint which conflicts with GitLab. We need to override it.

```yaml
stages:
  - build
  - test
  - deploy

build website:
    stage: build
    image: node:16-alpine
    script:
        - yarn install
        - yarn lint
        - CI=true yarn test src
        - yarn build
    artifacts:
        when: always
        paths:
            - build

test index.html:
    stage: test
    image: alpine
    script:
        test -f build/index.html

deploy to s3:
    stage: deploy
    image: 
        name: amazon/aws-cli:2.16.9
        entrypoint: [""]
    script: 
        - aws --version

```

## Unit 3. Lesson 5 - Uploading a file to S3

1. In AWS S3, navigate to bucket
2. Copy ARN `arn:aws:s3:::mkzin-20240616`
3. Create folder
4. Copy folder URL `s3://mkzin-20240616/test/`

This will not work because we need to set up permissions and provide credentials

```yaml
deploy to s3:
    stage: deploy
    image: 
        name: amazon/aws-cli:2.16.9
        entrypoint: [""]
    script: 
        - aws --version
        - echo "Hello S3" > test.txt
        - aws s3 cp test.txt s3://mkzin-20240616 /test.txt
```

## Unit 3. Lesson 6 - Masking & protecting variables

1. Create variable using GitLab UI - Go to Settings
2. CI/CD > Variables > add key, value
3. Protected variable: OFF

Protected variable can be used to ensure that no one has credentials to deploy to production from other branch, other than main/master

```yaml
deploy to s3:
    stage: deploy
    image: 
        name: amazon/aws-cli:2.16.9
        entrypoint: [""]
    script: 
        - aws --version
        - echo "Hello S3" > test.txt
        - aws s3 cp test.txt s3://$AWS_S3/test.txt
```

## Unit 3. Lesson 7 - Identity management with AWS IAM

1. AWS > Go to Services > IAM
2. Create new user > Disabled > Provide user access to the AWS Management Console
3. Set permissions > Attach policies directly > AmazonS3FullAccess
4. Create user
5. Open user > Security credentials > Create access key

Create variable for access key in GitLab

1. CI/CD > Variables > add key > AWS_ACCESS_KEY_ID
2. CI/CD > Variables > add key > AWS_SECRET_ACCESS_KEY
3. CI/CD > Variables > add key > AWS_DEFAULT_REGION > ap-southeast-2
4. No need to specify variables in pipeline. GitLab will pick up automatically.

## Unit 3. Lesson 8 - Uploading multiple files to S3

```yaml
stages:
  - build
  - test
  - deploy

build website:
    stage: build
    image: node:16-alpine
    script:
        - yarn install
        - yarn lint
        - CI=true yarn test src
        - yarn build
    artifacts:
        when: always
        paths:
            - build

test index.html:
    stage: test
    image: alpine
    script:
        test -f build/index.html

deploy to s3:
    stage: deploy
    image: 
        name: amazon/aws-cli:2.16.9
        entrypoint: [""]
    script: 
        - aws --version
        - aws s3 sync build s3://$AWS_S3 --delete
```

## Unit 3. Lesson 9 - Hosting a website on S3

By default, files uploaded to AWS S3 are not publicly available.

1. Properties > Enablre static website  hosting
2. Permissions > Disable "Block all public access"
3. Copy web address on Properties page: [website url](http://mkzin-20240616.s3-website-ap-southeast-2.amazonaws.com/)
4. Public policy > Add actions > S3 > (Get Object)

Public policy to allow read access <http://mkzin-20240616.s3-website-ap-southeast-2.amazonaws.com/>

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "PublicRead",
            "Effect": "Allow",
            "Principal": "*",
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::mkzin-20240616/*"
        }
    ]
}
```

## Unit 3. Lesson 10 - Controlling when jobs run

Normally we want

- build, test - when in feature branch, inside merge request
- deploy - after merge

``` yaml
stages:
  - build
  - test
  - deploy

build website:
    stage: build
    image: node:16-alpine
    script:
        - yarn install
        - yarn lint
        - CI=true yarn test src
        - yarn build
    artifacts:
        when: always
        paths:
            - build

test index.html:
    stage: test
    image: alpine
    script:
        test -f build/index.html

deploy to s3:
    stage: deploy
    image: 
        name: amazon/aws-cli:2.16.9
        entrypoint: [""]
    rules:
        - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
    script: 
        - aws --version
        - aws s3 sync build s3://$AWS_S3 --delete


```

## Unit 3. Lesson 11 - Post-deployment testing

``` yaml
stages:
  - build
  - test
  - deploy
  - post deploy

```

## Unit 3. Lesson 12 - What is CI/CD?

Next step is adding staging environment to ensure deployement is successfull before deploing to production.

![ci-cd](./attachments/U3L12/ci-cd.jpg)

## Unit 3. Lesson 13 - Assignment. Add staging environment

1. Add staging environment
2. add tests in Staging environment

Subtasks

1. Create S3 bucket for staging env
2. Create S3 bucket for prod env
3. Rename variable in GitLab to point to new buckets
4. Update pipeline to deploy to staging - automatically. Deploy to production manually
5. Add tests in staging env.

Description

1. Create S3 bucket for staging env

It is not possible to rename S3 buckets

- mkzin-staging: <http://mkzin-staging.s3-website-ap-southeast-2.amazonaws.com>
- mkzin-prod: <http://mkzin-prod.s3-website-ap-southeast-2.amazonaws.com>

2. Create production bucket  

3. Rename variable in GitLab to point to new buckets

AWS_S3_STAGING:mkzin-staging

4. Update pipeline to deploy to staging - automatically. Deploy to production manually

- deploy to staging rule: automatically, default branch only (main)
- deploy to production rule: manually, default branch only (main)

## Unit 3. Lesson 15 - Environments

1. GitLab project > Operate > Environments > Create an environment > Production
2. GitLab project > Operate > Environments > Create an environment >Staging
3. Settings > CI/CD > Variables > Select AWS_S3_PROD > Edit > Environments > production
4. Protect variable
5. Change key to AWS_S3
6. Repeat the same for Staging env

Continue this lesson

``` yaml

stages:
  - build
  - test
  - deploy staging
  - deploy production

variables:
    APP_VERSION: $CI_PIPELINE_IID

build website:
    stage: build
    image: node:16-alpine
    script:
        - yarn install
        - yarn lint
        - CI=true yarn test src
        - yarn build
        - echo $APP_VERSION > build/version.html
    artifacts:
        when: always
        paths:
            - build

test website:
    stage: test
    image: node:16-alpine
    script:
        - yarn global add serve
        - apk add curl
        - serve -s build &
        - sleep 10
        - curl http://localhost:3000 | grep "React App"

.deploy:
    image: 
        name: amazon/aws-cli:2.16.9
        entrypoint: [""]
    rules:
        - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
    script: 
        - cat /etc/*-release
        - uname -srm
        - aws --version
        - aws s3 sync build s3://$AWS_S3 --delete
        - curl $CI_ENVIRONMENT_URL | grep "React App"
        - curl $CI_ENVIRONMENT_URL/version.html | grep $APP_VERSION

deploy to staging:
    stage: deploy staging
    environment: staging
    extends: .deploy

deploy production:
    stage: deploy production
    environment: production
    extends: .deploy

```

## Unit 4. Lesson 1 - Unit overview

Dockerising application.
Deploying to AWS Elastic Beanstalk.

## Unit 4. Lesson 2 - Introduction to AWS Elastic Beanstalk

Elastic Beanstalk allows to deploy application in AWS cloud without having to worry about virtual server. No need to manage virtual machine.

Make sure you stop EB service when you do not use it actively. Otherwise you will be charged.

## Unit 4. Lesson 3 - Creating a new AWS Elastic Beanstalk application

   1. Log in AWS cnsole
   2. Search Elastic Beanstalk
   3. Create new application > Web server environment > name
   4. Platform > Doscker
   5. Application code > Sample application
   6. Configure service access > Create and use new service role > Default (aws-elasticbeanstalk-service-role)
   7. EC2 key pair - select in dropdown is available
   8. Set up networking, database, and tags  - skip
   9. Configure instance traffic and scaling - skip
   10. Configure updates, monitoring, and logging - System - basic, add Email notifications

In the background AWS creates resources: EC2 instance, S3 bucket.
The issue arises due to the security policies of AWS, which prevents Elastic Beanstalk from creating it's own instance profile. Now, an instance profile is required for an environment because it provides the necessary permissions and access rights to the EC2 instances that run your application.

 1. [done] Add user and attach 4 policies
 2. [done] Create role
 3. [done] Create a new environment

### Add user and attach 4 policies

I used existing user `gitlab`

1. Open Services > IAM
2. Users > gitlab > Attach policies directly
3. Add these 4 permissions
   1. AdministratorAccess,
   2. AWSElasticBeanstalkMulticontainerDocker,
   3. AWSElasticBeanstalkWebTier,
   4. AWSElasticBeanstalkWorkerTier
   5. Next > Add permissions

### Create role  

Create role `aws-eb-ec2-profile`

1. Open Services > IAM
2. Roles > Create role - follow this [guide](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/environments-create-wizard.html)
3. Add these 4 permissions
   1. AdministratorAccess,
   2. AWSElasticBeanstalkMulticontainerDocker,
   3. AWSElasticBeanstalkWebTier,
   4. AWSElasticBeanstalkWorkerTier
   5. Add permissions

### Create a new environment

1. Services > Elastic  Beanstalk >  Service access > Edit
2. Service role > `aws-eb-ec2-profile`

Create template  for AWS environment. This tells AWS which container to run

 ```json
 {
  "AWSEBDockerrunVersion": "1",
  "Image": {
    "Name": "nginx"
  },
  "Ports": [
    {
      "ContainerPort": 80
    }
  ]
}
 
 ```

 1. AWS console > Elastic Beanstalk > open environment > Upload and deploy
 2. Wait for EB to update environment

## Unit 4. Lesson 4 - Creating the Dockerfile

